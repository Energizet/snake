﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Snake
{
    class Program
    {
        static void Main(string[] args)
        {
            int complexity = 1;
            if (args.Length == 0)
            {
                Console.WriteLine("Please enter complexity: 0 = easy, 1 = normal, 2+ = hard");
                Console.Write("Complexity: ");
                complexity = 1; // 0 = easy; 1 = normal; 2 = hard
                try
                {
                    complexity = Convert.ToInt32(Console.ReadKey().KeyChar.ToString());
                }
                catch (Exception ex) { }
            }
            else if (args[0] == "-e")
            {
                complexity = 0;
            }
            else if (args[0] == "-n")
            {
                complexity = 1;
            }
            else if (args[0] == "-h")
            {
                complexity = 2;
            }

            int horisont = 2;
            int vertical = 0;

            Console.CursorVisible = false;

            Game game = new Game(horisont, vertical, complexity);
            Thread thread = new Thread(new ThreadStart(game.Run));
            thread.Start();

            ConsoleKey key = ConsoleKey.RightArrow;
            int side = 3;
            while (true)
            {
                if (game.SetTurn)
                {
                    switch (key = Console.ReadKey(true).Key)
                    {
                        case ConsoleKey.UpArrow:
                            if (side != 1)
                            {
                                horisont = 0;
                                vertical = -1;
                                side = 0;
                            }
                            break;
                        case ConsoleKey.DownArrow:
                            if (side != 0)
                            {
                                horisont = 0;
                                vertical = 1;
                                side = 1;
                            }
                            break;
                        case ConsoleKey.LeftArrow:
                            if (side != 3)
                            {
                                horisont = -2;
                                vertical = 0;
                                side = 2;
                            }
                            break;
                        case ConsoleKey.RightArrow:
                            if (side != 2)
                            {
                                horisont = 2;
                                vertical = 0;
                                side = 3;
                            }
                            break;
                    }
                    game.SetTurn = false;
                }
                if (game.GetGameOver())
                {
                    side = 3;

                    horisont = 2;
                    vertical = 0;
                    
                    game = new Game(horisont, vertical, complexity);
                    game.Clear();

                    thread = new Thread(new ThreadStart(game.Run));
                    thread.Start();
                }
                game.Set(horisont, vertical);
            }
        }
    }
}
