﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Snake
{
    class Game
    {
        int horisont;
        int vertical;

        int width;
        int height;

        int appleX;
        int appleY;

        Snake snake;
        bool gameover = false;

        bool setTurn = true;
        public bool SetTurn { get => setTurn; set => setTurn = value; }

        int speed;

        int complexity;

        public Game(int horisont, int vertical, int complexity)
        {
            this.complexity = complexity;
            Set(horisont, vertical);
            Console.Clear();
            Clear();
        }

        public void Set(int horisont, int vertical)
        {
            this.horisont = horisont;
            this.vertical = vertical;
        }

        public void Clear()
        {
            width = Console.WindowWidth;
            height = Console.WindowHeight;

            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    if (i == 0 || i == height - 1 || j == 0 || j == width - 1)
                    {
                        Console.SetCursorPosition(j, i);
                        Console.BackgroundColor = ConsoleColor.White;
                        Console.Write(' ');
                    }
                    else
                    {
                        Console.SetCursorPosition(j, i);
                        Console.BackgroundColor = ConsoleColor.Black;
                        Console.Write(' ');
                    }
                }
            }
            Console.SetCursorPosition(0, 0);
        }

        void Apple()
        {
            appleX = new Random().Next(1, width / 2 - 1);
            appleY = new Random().Next(1, height - 1);
            Console.SetCursorPosition(appleX *= 2, appleY);
            Console.BackgroundColor = ConsoleColor.Red;
            Console.Write("  ");
            if (speed > 0)
            {
                int fps = 1000 / speed;
                fps += complexity;
                speed = 1000 / fps;
            }
        }

        public void Run()
        {
            this.snake = new Snake(new int[][] {
                    new int[] { 10, 10 },
                    new int[] { 10, 10 },
                    new int[] { 10, 10 },
                    new int[] { 10, 10 },
                    new int[] { 10, 10 },
                });

            Apple();
            speed = 100;

            while (!gameover)
            {
                int[][] snakePos = snake.GetPos();
                for (int i = 0; i < snakePos.Length; i++)
                {
                    Console.SetCursorPosition(snakePos[i][0], snakePos[i][1]);
                    Console.BackgroundColor = ConsoleColor.Black;
                    Console.Write("  ");
                }

                int[][] snakeTemp = new int[snakePos.Length][];
                snakeTemp[0] = new int[] { snakePos[0][0], snakePos[0][1] };
                snakeTemp[0][0] += horisont;
                snakeTemp[0][1] += vertical;
                for (int i = 1; i < snakePos.Length; i++)
                {
                    snakeTemp[i] = new int[] { snakePos[i - 1][0], snakePos[i - 1][1] };
                }

                if (snakeTemp[0][0] == appleX && snakeTemp[0][1] == appleY)
                {
                    Apple();
                    int[][] snakeTemp2 = new int[snakeTemp.Length + 1][];
                    Array.Copy(snakeTemp, snakeTemp2, snakeTemp.Length);
                    snakeTemp2[snakeTemp2.Length - 1] = new int[] { snakeTemp[snakeTemp.Length - 1][0], snakeTemp[snakeTemp.Length - 1][1] };
                    snakeTemp = snakeTemp2;
                }

                for (int i = 1; i < snakeTemp.Length; i++)
                {
                    if (snakeTemp[0][0] == snakeTemp[i][0] && snakeTemp[0][1] == snakeTemp[i][1])
                    {
                        gameover = true;
                    }
                }

                if (snakeTemp[0][0] == 0 || snakeTemp[0][0] == width - 2 || snakeTemp[0][1] == 0 || snakeTemp[0][1] == height - 1)
                {
                    gameover = true;
                }

                for (int i = 0; i < snakeTemp.Length; i++)
                {
                    Console.SetCursorPosition(snakeTemp[i][0], snakeTemp[i][1]);
                    Console.BackgroundColor = ConsoleColor.White;
                    Console.Write("  ");
                    Console.SetCursorPosition(appleX, appleY);
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.Write("  ");
                }
                snake.SetPos(snakeTemp);
                setTurn = true;

                if (gameover)
                {
                    GameOver();
                }

                Thread.Sleep(speed);
            }
        }

        void GameOver()
        {
            gameover = true;
            Console.SetCursorPosition(width / 2 - 5, height / 2);
            Console.BackgroundColor = ConsoleColor.Black;
            Console.WriteLine("Game Over!");
            Console.SetCursorPosition(0, height-1);
            Console.ResetColor();
        }

        public bool GetGameOver()
        {
            return gameover;
        }
    }
}
