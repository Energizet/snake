﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Snake
{
    class Snake
    {
        int[][] pos;

        public Snake(int[][] pos)
        {
            this.pos = pos;
        }

        public void SetPos(int[][] pos)
        {
            this.pos = pos;
        }

        public int[][] GetPos()
        {
            return pos;
        }

        public int[] GetLastPos()
        {
            return pos[pos.Length - 1];
        }
    }
}
